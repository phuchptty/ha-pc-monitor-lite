import psutil
from function import *
import paho.mqtt.client as mqtt
import time
import json
import os
import PySimpleGUIWx as sg
from win10toast import ToastNotifier
from threading import Thread

# tray icon
tray_menu = ['BLANK', ['&Trạng Thái', '---', '&Cài Đặt', '&Thoát']]
tray = sg.SystemTray(menu=tray_menu, filename=r'computer.ico', tooltip="Hass PC Monitor Lite")

def noti():
    if getPlatform() == "Windows":
        toaster = ToastNotifier()
        toaster.show_toast("Hass PC Monitor Lite","Ứng Dụng Đang Chạy Nền",icon_path=r"computer.ico",duration=5)
Thread(target=noti).start()


# read cf file
with open('config.json') as f:
    data = json.load(f)

mqtt_broker = data['mqtt']['broker']
mqtt_username = data['mqtt']['username']
mqtt_password = data['mqtt']['password']
mqtt_port = int(data['mqtt']['port'])
mqtt_pcname = data['customPCName']

cpuChannel = mqtt_pcname + data['channel']['cpu']
ramChannel = mqtt_pcname + data['channel']['ram']
diskChannel = mqtt_pcname + data['channel']['disk']
batteryChannel = mqtt_pcname + data['channel']['battery']


# config mqtt
def on_connect_callback(client, userdata, flags, rc):
    print("CONNACK received with code {}.".format(rc))

def on_publish(client,userdata,result):
    print("Đã Public Data")
    pass

client = mqtt.Client("HassMQTTMonitorClient")
client.on_connect = on_connect_callback
client.on_publish = on_publish
print("Thêm Tài Khoản")
client.username_pw_set(mqtt_username, mqtt_password)
print("Kết Nối Tới MQTT")
client.connect(mqtt_broker, mqtt_port)
client.loop_start()

while True:
    # get data
    cpuCore = getTotalCPUCore(False)
    cpuCoreLogical = getTotalCPUCore(True)
    cpuPercent = getCPUPercent(False)
    cpuFreq = getCPUFreg(False).current
    ramTotal = getRamTotal('GB')
    ramUsed = getRamUsed('GB')
    ramPercent = getRamPercent()
    diskUsage = getDiskUsage('/', 'GB')
    diskToal = getDiskTotal('/', 'GB')
    diskPercent = getDiskPercent('/')
    battery = getBatteryState().percent
    batteryStatus = getBatteryState().power_plugged

    # optimed data
    cpuData = json.dumps({'realCore': cpuCore, 'thread': cpuCoreLogical,'pLoad': cpuPercent,'freg': cpuFreq})
    ramData = json.dumps({'ramTotal': ramTotal, 'ramUsed': ramUsed, 'ramPercent': ramPercent})
    diskData = json.dumps({'diskUsage': diskUsage, 'diskTotal': diskToal, 'diskPercent': diskPercent})
    batteryData = json.dumps({'percent': battery, 'status': batteryStatus})

    # Excute Tray Icon
    print('Khởi Tạo Icon')
    tray_item = tray.Read(timeout=0)
    if tray_item == 'Thoát':
        os._exit(1)
    elif tray_item == 'Cài Đặt':
        os.startfile('config.json', 'open')
        os._exit(1)
    elif tray_item == 'Trạng Thái':
        layout = [
            [sg.Text('CPU: {} Core | {} Thread'.format(cpuCore, cpuCoreLogical))],
            [sg.Text('CPU LOAD: {} % | XUNG NHỊP: {} MHz'.format(cpuPercent, cpuFreq))],
            [sg.Text('RAM: {}/{} GB | {} %'.format(ramUsed, ramTotal,ramPercent ))],
            [sg.Text('Ổ ĐĨA: {}/{} GB | {} %'.format(diskUsage,diskToal,diskPercent))],
            [sg.Text('PIN:')],
            [sg.Text('  Dung Lượng: {} %'.format(battery))],
            [sg.Text('  Trạng Thái: {}'.format(batteryStatus))],
            [sg.Text('-----------------------------------------------------------')],
            [sg.Exit()]
        ]

        window = sg.Window('Trạng Thái', icon=r"computer.ico", size=(600, 250)).Layout(layout)
        event, values = window.Read()
        if event is None or event == 'Exit':
            window.Close()

    #cpu
    client.publish(cpuChannel,cpuData)
    #ram
    client.publish(ramChannel, ramData)
    #disk
    client.publish(diskChannel, diskData)
    #battery
    client.publish(batteryChannel, batteryData)

    time.sleep(1)
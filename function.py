import psutil
import platform

#CPUU
def getTotalCPUCore(logical=True):
	if (logical == False):
		return psutil.cpu_count(logical=False)
	elif (logical != False):
		return psutil.cpu_count()
	else:
		return False

def getCPUPercent(percpu=False):
	if (percpu == False):
		return psutil.cpu_percent(interval=1, percpu=False)
	elif (percpu != False):
		return psutil.cpu_percent(interval=1, percpu=True)
	else:
		return False

def getCPUFreg(percpu=False):
	if (percpu == False):
		return psutil.cpu_freq(percpu=False)
	elif (percpu != False):
		return psutil.cpu_freq(percpu=True)
	else:
		return False

#RAMMM
def getRamTotal(unit='MB'):
	mem = int(psutil.virtual_memory()[0])
	if (unit == 'MB'):
		return round(mem/1024/1024,2);
	elif (unit=='KB'):
		return round(mem/1024,2);
	elif (unit=='GB'):
		return round(mem/1024/1024/1024,2);
	elif (unit=='B'):
		return round(mem4,2);

def getRamPercent():
	return round((getRamUsed('MB')/getRamTotal('MB'))*100,2)

def getRamUsed(unit='MB'):
	mem = int(psutil.virtual_memory()[3])
	if (unit == 'MB'):
		return round(mem/1024/1024,2);
	elif (unit=='KB'):
		return round(mem/1024,2);
	elif (unit=='GB'):
		return round(mem/1024/1024/1024,2);
	elif (unit=='B'):
		return round(mem4,2);

def getRamFree(unit='MB'):
	mem = int(psutil.virtual_memory()[4])
	if (unit == 'MB'):
		return round(mem/1024/1024,2);
	elif (unit=='KB'):
		return round(mem/1024,2);
	elif (unit=='GB'):
		return round(mem/1024/1024/1024,2);
	elif (unit=='B'):
		return round(mem4,2);

#SWAPPP
def getSwapTotal(unit='MB'):
	mem = int(psutil.virtual_memory()[0])
	if (unit == 'MB'):
		return round(mem/1024/1024,2);
	elif (unit=='KB'):
		return round(mem/1024,2);
	elif (unit=='GB'):
		return round(mem/1024/1024/1024,2);
	elif (unit=='B'):
		return round(mem4,2);

def getSwapPercent(unit='MB'):
	mem = int(psutil.virtual_memory()[2])
	if (unit == 'MB'):
		return round(mem/1024/1024,2);
	elif (unit=='KB'):
		return round(mem/1024,2);
	elif (unit=='GB'):
		return round(mem/1024/1024/1024,2);
	elif (unit=='B'):
		return round(mem4,2);

def getSwapUsed(unit='MB'):
	mem = int(psutil.virtual_memory()[3])
	if (unit == 'MB'):
		return round(mem/1024/1024,2);
	elif (unit=='KB'):
		return round(mem/1024,2);
	elif (unit=='GB'):
		return round(mem/1024/1024/1024,2);
	elif (unit=='B'):
		return round(mem4,2);

def getSwapFree(unit='MB'):
	mem = int(psutil.virtual_memory()[4])
	if (unit == 'MB'):
		return round(mem/1024/1024,2);
	elif (unit=='KB'):
		return round(mem/1024,2);
	elif (unit=='GB'):
		return round(mem/1024/1024/1024,2);
	elif (unit=='B'):
		return round(mem4,2);

#DISKK
def getListDisk():
	return psutil.disk_partitions(all=True)

def getDiskUsage(path,unit):
	mem = psutil.disk_usage(path).used
	if (unit == 'MB'):
		return round(mem/1024/1024,2);
	elif (unit=='KB'):
		return round(mem/1024,2);
	elif (unit=='GB'):
		return round(mem/1024/1024/1024,2);
	elif (unit=='B'):
		return round(mem4,2);

def getDiskTotal(path,unit):
	mem = psutil.disk_usage(path).total
	if (unit == 'MB'):
		return round(mem/1024/1024,2);
	elif (unit=='KB'):
		return round(mem/1024,2);
	elif (unit=='GB'):
		return round(mem/1024/1024/1024,2);
	elif (unit=='B'):
		return round(mem4,2);

def getDiskPercent(path):
	return psutil.disk_usage(path).percent

#PLATFORM
def getPlatform():
	pf = platform.system()
	return pf

#TEMP
def getCPUTemp():
	if getPlatform() != 'Linux':
		return False
	else:
		return psutil.sensors_temperatures()

def getCPUFanSpeed():
	if getPlatform() == 'Windows':
		return False
	else:
		return psutil.sensors_fans()

def getBatteryState():
	return psutil.sensors_battery()

def getSystemBootTime():
	return psutil.boot_time()